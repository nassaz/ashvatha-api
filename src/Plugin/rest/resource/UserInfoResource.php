<?php

namespace Drupal\ashvatha_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;

/**
 * Provides a resource
 *
 * @RestResource(
 *   id = "user_info_resource",
 *   label = @Translation("User Info Resource"),
 *   entity_type = "user",
 *   serialization_class = "Drupal\user\Entity\User",
 *   uri_paths = {
 *     "canonical" = "/api/user/{user}",
 *   }
 * )
 */
class UserInfoResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        global $base_url;
        $uid = \Drupal::currentUser()->id();
        $user = \Drupal\user\Entity\User::load($uid);

         if($entity->hasPermission('join group', $user)) {

         }
        $groups = array();
		$grp_membership_service = \Drupal::service('group.membership_loader');
		$grps = $grp_membership_service->loadByUser($entity);
		foreach ($grps as $grp) {
		        $groups[] = [
		            'id' => $grp->getGroup()->id(),
                    'title' => $grp->getGroup()->label(),
                    ];
		}

        if (!$entity->get('user_picture')->isEmpty()) {
            $picture = $entity->get('user_picture')->entity->url();
        } else {
            $picture = $base_url . '/sites/default/files/styles/thumbnail/public/default_images/Ashvatha-Logo.png';
        }

        $response = [
            'groups' => $groups,
            'name' => $entity->getUsername(),
            'mail' => $entity->getEmail(),
            'last_access' => $entity->getLastAccessedTime(),
            'roles' => $entity->getRoles(),
            'uid' => $entity->id(),
            'language' => $entity->getPreferredLangcode(),
            'user_picture' => $picture,
            'field_info' => $entity->get('field_info')[0]->processed
        ];

        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
        //return new ResourceResponse($entity);
    }

}