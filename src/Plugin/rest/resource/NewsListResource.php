<?php

namespace Drupal\ashvatha_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "news_list_resource",
 *   label = @Translation("News List Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/news",
 *   }
 * )
 */
class NewsListResource extends ResourceBase
{

    /**
     * Responds to GET requests.
     *
     * @return ResourceResponse
     */
    public function get()
    {
        $response = [];
        $query = \Drupal::entityQuery('node')
            ->condition('status', 1)
            ->condition('type', 'article')
            ->sort('created', 'DESC')
            ->range(0, 10)
            ->accessCheck(false);

        $news_ids = $query->execute();
        $news = Node::loadMultiple($news_ids);
        foreach ($news as $item) {
            //\Drupal::logger('demo_resource2')->notice();
            if (!$item->get('field_image')->isEmpty()) {
                $imageURL = ImageStyle::load('large')->buildUrl($item->get('field_image')->entity->uri->value);
            } else {
                $imageURL = '';
            }
            if (!$item->get('field_license')->isEmpty()) {
                $license = $item->get('field_license')->entity->getName();
            } else {
                $license = '';
            }
            if (!$item->get('field_original_article')->isEmpty()) {
                $original = $item->get('field_original_article')[0]->uri;
            } else {
                $original = '';
            }
            $newsData = [
                'id' => $item->id(),
                'name' => $item->label(),
                'body' => $item->get('body')[0]->processed,
                'image' => $imageURL,
                'video' => $item->get('field_video_embed')->getValue(),
                'sourceURL' => $original,
                'license' => $license
            ];
            $response[] = $newsData;

        }

        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
        //return new ResourceResponse($entity);
    }

}