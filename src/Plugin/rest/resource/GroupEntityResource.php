<?php

namespace Drupal\ashvatha_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "group_entity_resource",
 *   label = @Translation("Group Entity Resource"),
 *   entity_type = "group",
 *   serialization_class = "Drupal\group\Entity\Group",
 *   uri_paths = {
 *     "canonical" = "/api/group/{group}",
 *     "https://www.drupal.org/link-relations/create" = "/api/group"
 *   }
 * )
 */
class GroupEntityResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        $groupMembers = $entity->getContentEntities('group_membership');
        $groupArticles = $entity->getContentEntities('group_node:article');
        $groupPages = $entity->getContentEntities('group_node:page');
        $memberList = [];
        $articleList = [];
        $pageList = [];
        foreach($groupMembers as $member) {
            $memberItem['name'] = $member->label();
            $memberItem['id'] = $member->id();
            $memberList[] = $memberItem;
        }
        foreach($groupArticles as $article) {
            if (!$article->get('field_image')->isEmpty()) {
                $imageURL = ImageStyle::load('large')->buildUrl($article->get('field_image')->entity->uri->value);
            } else {
                $imageURL = '';
            }
            if (!$article->get('field_license')->isEmpty()) {
                $license = $article->get('field_license')->entity->getName();
            } else {
                $license = '';
            }
            $articleItem['name'] = $article->label();
            $articleItem['id'] = $article->id();
            $articleItem['body'] = $article->get('body')[0]->processed;
            $articleItem['license'] = $license;
            $articleItem['image'] = $imageURL;
            $articleItem['video'] = $article->get('field_video_embed')->getValue();
            $articleList[] = $articleItem;
        }
        foreach($groupPages as $page) {
            $pageItem['name'] = $page->label();
            $pageItem['id'] = $page->id();
            $pageList[] = $pageItem;
        }
        $response = [
            'id' => $entity->id(),
            'name' => $entity->label(),
            'description' => $entity->get('field_description')->getValue(),
            'image' => $entity->get('field_image')->getValue(),
            'website' => $entity->get('field_website')->getValue(),
            'userCount' => count($groupMembers),
            'members' => $memberList,
            'articles' => $articleList,
            'pages' => $pageList
        ];
        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
        //return new ResourceResponse($entity);
    }

    /**
     * Responds to POST requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     * @return ResourceResponse
     */
    public function post(EntityInterface $entity = NULL)
    {

        parent::post($entity);
        return new ResourceResponse($entity);
    }

    /**
     * Responds to PATCH requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface $original_entity
     *   The original entity.
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The current entity.
     */
    public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL)
    {

        parent::patch($original_entity, $entity);

        return new ResourceResponse($entity, 200);
    }
}