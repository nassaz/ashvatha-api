<?php

namespace Drupal\ashvatha_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;
use Drupal\node\Entity\Node;
use Drupal\image\Entity\ImageStyle;

global $base_url;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "myinfo_resource",
 *   label = @Translation("My Info Resource"),
 *   serialization_class = "",
 *   uri_paths = {
 *     "canonical" = "/api/myinfo",
 *   }
 * )
 */
class MyInfoResource extends ResourceBase
{

    /**
     * Responds to GET requests.
     *
     * @return ResourceResponse
     */
    public function get()
    {
        global $base_url;
        $uid = \Drupal::currentUser()->id();
        $user = \Drupal\user\Entity\User::load($uid);

        $groups = array();
        $grp_membership_service = \Drupal::service('group.membership_loader');
        $grps = $grp_membership_service->loadByUser($user);
        foreach ($grps as $grp) {
            $groups[] = [
                'id' => $grp->getGroup()->id(),
                'title' => $grp->getGroup()->label(),
            ];
        }

        if (!$user->get('user_picture')->isEmpty()) {
            $picture = $user->get('user_picture')->entity->url();
        } else {
            $picture = $base_url . '/sites/default/files/styles/thumbnail/public/default_images/Ashvatha-Logo.png';
        }

        $response = [
            'groups' => $groups,
            'name' => $user->getUsername(),
            'mail' => $user->getEmail(),
            'last_access' => $user->getLastAccessedTime(),
            'roles' => $user->getRoles(),
            'uid' => $user->id(),
            'language' => $user->getPreferredLangcode(),
            'user_picture' => $picture,
            'field_info' => $user->get('field_info')[0]->processed
        ];

        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
        //return new ResourceResponse($entity);
    }

}