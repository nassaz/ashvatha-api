<?php

namespace Drupal\ashvatha_api\Plugin\rest\resource;

use Drupal\Core\Entity\EntityInterface;
use Drupal\rest\Plugin\rest\resource\EntityResource;
use Drupal\rest\ResourceResponse;
use Drupal\group\Entity;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\GroupMembership;
use Drupal\image\Entity\ImageStyle;

/**
 * Provides a resource to get and patch asset type terms
 *
 * @RestResource(
 *   id = "news_entity_resource",
 *   label = @Translation("News Entity Resource"),
 *   entity_type = "node",
 *   serialization_class = "Drupal\node\Entity\Node",
 *   uri_paths = {
 *     "canonical" = "/api/news/{node}",
 *     "https://www.drupal.org/link-relations/create" = "/api/create/news"
 *   }
 * )
 */
class NewsEntityResource extends EntityResource
{

    /**
     * Responds to GET requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     *
     * @return ResourceResponse
     */
    public function get(EntityInterface $entity = NULL)
    {
        if (!$entity->get('field_image')->isEmpty()) {
            $imageURL = ImageStyle::load('large')->buildUrl($entity->get('field_image')->entity->uri->value);
        } else {
            $imageURL = '';
        }
        if (!$entity->get('field_license')->isEmpty()) {
            $license = $entity->get('field_license')->entity->getName();
        } else {
            $license = '';
        }
        if (!$entity->get('field_original_article')->isEmpty()) {
            $original = $entity->get('field_original_article')[0]->uri;
        } else {
            $original = '';
        }
        $response = [
            'id' => $entity->id(),
            'name' => $entity->label(),
            'body' => $entity->get('body')[0]->processed,
            'image' => $imageURL,
            'video' => $entity->get('field_video_embed')->getValue(),
            'sourceURL' => $original,
            'license' => $license
        ];

        $build = array(
            '#cache' => array(
                'max-age' => 0,
            ),
        );
        return (new ResourceResponse($response))->addCacheableDependency($build);
        //return new ResourceResponse($response);
    }

    /**
     * Responds to POST requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The entity.
     * @return ResourceResponse
     */
    public function post(EntityInterface $entity = NULL)
    {

        parent::post($entity);
        return new ResourceResponse($entity);
    }

    /**
     * Responds to PATCH requests.
     *
     * @param \Drupal\Core\Entity\EntityInterface $original_entity
     *   The original entity.
     * @param \Drupal\Core\Entity\EntityInterface|null $entity
     *   The current entity.
     *
     * @return ResourceResponse
     */
    public function patch(EntityInterface $original_entity, EntityInterface $entity = NULL)
    {

        parent::patch($original_entity, $entity);

        return new ResourceResponse($entity, 200);
    }
}